import './App.css';
import SearchPanel from './SearchPanel/SearchPanel';

function App() {
  return <div className="App"><SearchPanel></SearchPanel></div>;
}

export default App;
