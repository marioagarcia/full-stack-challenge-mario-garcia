import { Radio, RadioChangeEvent } from 'antd';
import React from 'react';

type Props = {
  setCategory: (category: string) => void;
  category: string | null;
};

const CategoryRadioGroup: React.FC<Props> = ({ setCategory, category }) => {
  const handleCategoryChange = (e: RadioChangeEvent) => {
    setCategory(e.target.value);
  };
  return (
    <Radio.Group value={category} onChange={handleCategoryChange}>
      {['characters', 'planets'].map((category) => (
        <Radio.Button key={category} value={category}>{category}</Radio.Button>
      ))}
    </Radio.Group>
  );
};

export default CategoryRadioGroup;
