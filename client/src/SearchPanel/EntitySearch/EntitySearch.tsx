import React from 'react';
import { Input } from 'antd';

const { Search } = Input;

type Props = {
  setSearch: (category: string) => void;
  search: string;
  category: string | null;
};

const EntitySearch: React.FC<Props> = ({ setSearch, search, category }) => {
  const onSearch = (value: string) => setSearch(value);
  return (
    <Search
      placeholder={`Enter a ${category} name`}
      onSearch={onSearch}
      onChange={(e) => setSearch(e.target.value)}
      enterButton
    />
  );
};

export default EntitySearch;
