import React, { useEffect, useState } from 'react';
import { Card, Col, Table } from 'antd';

type Props = {
  search: string;
  category: string | null;
  planets: Array<Planet> | null;
  characters: Array<Character> | null;
};

const ResultsList: React.FC<Props> = ({
  search,
  category,
  planets,
  characters,
}) => {
  const [results, setResults] = useState<Array<Character | Planet>>([]);
  const [selectedEntity, setSelectedEntity] = useState<
    Character | Planet | null
  >(null);
  useEffect(() => {
    let selectedCategory = category == 'planets' ? planets : characters;
    selectedCategory = selectedCategory?.map((entity) => ({
      ...entity,
      key: entity.name,
    }));
    const filteredResults =
      selectedCategory?.filter((entity) => {
        return entity.name.toLowerCase().includes(search.toLowerCase());
      }) || [];
    setResults(search ? filteredResults : []);
    setSelectedEntity(null);
  }, [category, search]);

  const columns = [
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
      render: (name: string, entity: Planet | Character) => (
        <span
          style={{ cursor: 'pointer' }}
          onClick={() => setSelectedEntity(entity)}
        >
          {name}
        </span>
      ),
    },
    {
      title: 'Number of Films',
      dataIndex: 'films',
      key: 'films',
      render: (films: Array<string>) => <span>{films.length}</span>,
    },
  ];

  return (
    <>
      <Col span={12}>
        <Table
          style={{ width: '100%' }}
          dataSource={results}
          columns={columns}
        />
      </Col>
      <Col span={12}>
        <Card title={'Info'}>
          {selectedEntity && category == 'planets' && (
            <>
              <p>Name: {selectedEntity.name}</p>
              <p>Number of Films: {selectedEntity.films.length}</p>
              <p>Population: {(selectedEntity as Planet).population}</p>
              <p>Climate: {(selectedEntity as Planet).climate}</p>
              <p>Terrain: {(selectedEntity as Planet).terrain}</p>
              <p>Gravity: {(selectedEntity as Planet).gravity}</p>
            </>
          )}
          {selectedEntity && category == 'characters' && (
            <>
              <p>Name: {selectedEntity.name}</p>
              <p>Number of Films: {selectedEntity.films.length}</p>
              <p>Population: {(selectedEntity as Character).birth_year}</p>
              <p>Climate: {(selectedEntity as Character).gender}</p>
              <p>Terrain: {(selectedEntity as Character).height}</p>
              <p>Gravity: {(selectedEntity as Character).mass}</p>
            </>
          )}
        </Card>
      </Col>
    </>
  );
};

export default ResultsList;
