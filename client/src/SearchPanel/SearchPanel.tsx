import { Card, Row } from 'antd';
import { useEffect, useState } from 'react';
import CategoryRadioGroup from './CategoryRadioGroup/CategoryRadioGroup';
import EntitySearch from './EntitySearch/EntitySearch';
import ResultsList from './ResultsList/ResultsList';

const url = 'http://localhost:4000';

export default function SearchPanel({}) {
  const [characters, setCharacters] = useState<Array<Character> | null>(null);
  const [planets, setPlanets] = useState<Array<Planet> | null>(null);
  const [category, setCategory] = useState<string | null>('characters');
  const [search, setSearch] = useState<string>('');

  useEffect(() => {
    (async function getData() {
      const planets = await fetch(`${url}/planets`).then((res) => res.json());
      const characters = await fetch(`${url}/people`).then((res) => res.json());
      setPlanets(planets);
      setCharacters(characters);
    })();
  }, []);

  return (
    <Card title={'Star Wars API'} style={{ width: '800px' }}>
      <Row>
        <CategoryRadioGroup
          setCategory={setCategory}
          category={category}
        ></CategoryRadioGroup>
      </Row>
      <br></br>
       <Row>
        <EntitySearch
          setSearch={setSearch}
          search={search}
          category={category}
        ></EntitySearch>
      </Row>
      <br></br>
     <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
        <ResultsList category={category} search={search} planets={planets} characters={characters}></ResultsList>
      </Row> 
    </Card>
  );
}
