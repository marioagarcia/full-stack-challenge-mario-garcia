type Character = {
  name: string;
  height: string;
  mass: string;
  hair_color: string;
  skin_color: string;
  eye_color: string;
  birth_year: string;
  gender: string;
  films: Array<string>;
  key: string;
};

type Planet = {
  name: string;
  diameter: string;
  climate: string;
  gravity: string;
  terrain: string;
  population: string;
  residents: Array<string>;
  films: Array<string>;
  key: string;
};
