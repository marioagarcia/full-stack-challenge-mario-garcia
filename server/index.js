/*
    This is just a small sample to get you started. 
    Note that the docker setup will be looking for `index.js`,
    so it's best to use this file or the same file name.
 */
const express = require('express');
const cors = require('cors');

const swapi = 'https://swapi.dev/api/';

const PORT = process.env.PORT || 4000;
const app = express();
const axios = require('axios');
const sortBy = require('lodash/sortBy');

app.use(cors());

app.get('/', (req, res) => res.send('Hello World!'));

app.get('/people', async (req, res) => {
  let query = `${swapi}/people`;
  let all = await getAll(query);

  const field = req.query.sortBy;

  if (field && ['name', 'height', 'mass'].includes(field)) {
    all = sortBy(all, (p) => (field == 'name' ? p[field] : +p[field]));
  }

  res.send(all);
});

app.get('/planets', async (req, res) => {
  const allPlanets = await getAll(`${swapi}/planets`);

  for (let planet of allPlanets) {
    const residentsPromises = planet.residents.map((residentUrl) =>
      axios(residentUrl)
    );
    const residentResponses = await Promise.all(residentsPromises);
    planet.residents = residentResponses.map(
      (residentResponse) => residentResponse.data.name
    );
  }

  res.send(allPlanets);
});

app.listen(PORT, () => console.log(`Example app listening on port ${PORT}!`));

async function getAll(query) {
  let all = [];
  while (query) {
    const response = await axios(query);
    const { next, results } = response.data;
    all.push(...results);
    if (next) {
      query = next;
    } else {
      query = '';
    }
  }
  return all;
}
